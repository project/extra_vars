<?php

/**
 * Implements hook_variable_group_info().
 */
function extra_vars_variable_group_info() {
  $extra_groups = extra_vars_load_groups();
  $groups = array();
  foreach ($extra_groups as $gid => $group) {
    $groups['extra_vars_' . $gid] = array(
      'title' => $group->name,
      'access' => 'use extra vars',
    );
  }
  return $groups;
}

/**
 * Implements hook_variable_info().
 */
function extra_vars_variable_info($options) {
  $variables = array();
  $fields = extra_vars_load_fields();
  foreach ($fields as $fid => $field) {
    $variables['extra_vars:' . $field->machine_name] = array(
      'type' => $field->type,
      'title' => $field->name,
      'description' => $field->description,
      'required' => TRUE,
      'group' => 'extra_vars_' . $field->gid,
    );
  }
  return $variables;
}

/**
 * Implements hook_variable_type_info().
 */
function extra_vars_variable_type_info() {
  // File object
  $type['extra_vars_file'] = array(
    'title' => t('File upload (managed file)'),
    'element' => array('#type' => 'managed_file'),
    'element callback' => 'extra_vars_form_element_file_managed',
    'format callback' => 'extra_vars_format_file',
    'submit callback' => 'extra_vars_form_file_managed_submit',
    'file managed' => TRUE,
    'upload location' => 'public://extra_vars/',
    'validate size' => file_upload_max_size(),
  );
  return $type;
}

/**
 * Build upload file variable.
 */
function extra_vars_form_element_file_managed($variable, $options) {
  $element = variable_form_element_default($variable, $options);
  $element = _exrta_vars_form_element($variable, $options, $element);
  $element['#description'] = theme('file_upload_help', array('description' => $element['#description'], 'upload_validators' => $element['#upload_validators']));
  return $element;
}

/**
 * Build upload file element.
 */
function _exrta_vars_form_element($variable, $options, $element = array()) {
  if (isset($variable['validate extensions'])) {
    $element['#upload_validators']['file_validate_extensions'] = array($variable['validate extensions']);
  }
  if (isset($variable['validate size'])) {
    $element['#upload_validators']['file_validate_size'] = array(parse_size($variable['validate size']));
  }
  $element['#upload_location'] = $variable['upload location'];

  return $element;
}

/**
 * Variable format callback. Generic file.
 */
function extra_vars_format_file($variable, $options = array()) {
  if ($path = _extra_vars_get_path($variable)) {
    $url = file_create_url($path);
    return l($url, $url);
  }
  else {
    return t('No file');
  }
}

/**
 * Get file path.
 */
function _extra_vars_get_path($variable) {
  if (!empty($variable['value'])) {
    if (!empty($variable['file managed'])) {
      $file = file_load($variable['value']);
      return $file ? $file->uri : '';
    }
    else {
      return $variable['value'];
    }
  }
}

/**
 * Managed file, submit callback.
 */
function extra_vars_form_file_managed_submit($variable, $options, $form, &$form_state) {
  global $user;
  $name = $variable['name'];
  $value = variable_get_value($variable);
  // Managed files, more complex handling.
  if ($uploaded = $form_state['values'][$name]) {
    $file = file_load($uploaded);
    if ($file && !$file->status) {
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
      file_usage_add($file, 'extra_vars', 'extra_vars', $user->uid);
      // Remove old file if existing.
      if ($value) {
        _extra_vars_upload_remove_file($name);
      }
    }
  }
}

/**
 * Remove old file with element key.
 *
 * @param string $name
 *   An associative string of variable name.
 */
function _extra_vars_upload_remove_file($name) {
  $fid = variable_get_value($name);
  if ($fid && $file = file_load($fid)) {
    file_usage_delete($file, 'extra_vars', 'extra_vars', 1);
    file_delete($file);
  }
}
