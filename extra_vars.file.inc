<?php

/**
 * @file
 *    Contain Extra vars file field value callback function.
 */
function extra_vars_get_file_field_value($field_name, $value, $type, $view_mode) {
  $file = file_load($value);
  switch ($view_mode) {
    case 'page':
      $return_value = file_create_url($file->uri);
      break;
    case 'admin':
      $options = array(
        'attributes' => array('target' => '_blank'),
      );
      $path = file_create_url($file->uri);
      $return_value = l($file->filename, $path, $options);
      break;
    case 'file':
      $return_value = $file;
      break;
    default:
      $return_value = $value;
  }
  return $return_value;
}
