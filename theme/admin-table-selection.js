
/**
 * @file
 * Contain behavior for selection text when focused helper input.
 */
Drupal.behaviors.extraVarsAdminTableSelection = {
  attach: function (context, settings) {
    var title = Drupal.t('Click for select and copy by press Ctrl + C (or Command + C for Mac OS)');
    jQuery('.extra-vars-selection').once('extra-vars-selection').each(function (i, d) {
      var $element = jQuery(d);
      $element.attr('title', title);
      $element.focus(function () {
        $element.select();
      });
    });
  }
};