<?php

/**
 * @file 
 *   Contain ExtraVars helper class.
 */
class ExtraVars {

  protected $fields;
  protected $extra_vars;

  public function __construct() {
    foreach (extra_vars_load_all_fields() as $field) {
      $this->fields[$field->machine_name] = $field->type;
    }
  }

  public function __get($name_raw) {
    if (empty($this->extra_vars[$name_raw])) {
      $this->extra_vars[$name_raw] = $this->_getValue($name_raw);
      if (empty($this->extra_vars[$name_raw]) && strpos($name_raw, '-')) {
        $new_name = str_replace('-', '_', $name_raw);
        $this->extra_vars[$name_raw] = $this->_getValue($new_name);
      }
      if (empty($this->extra_vars[$name_raw]) && strpos($name_raw, '_')) {
        $new_name = str_replace('_', '-', $name_raw);
        $this->extra_vars[$name_raw] = $this->_getValue($new_name);
      }
    }
    return $this->extra_vars[$name_raw];
  }

  private function _getValue($name_raw) {
    $name_parts = explode(':', $name_raw);
    $name = array_shift($name_parts);
    if (!empty($name_parts)) {
      $view_mode = array_shift($name_parts);
    }
    else {
      $view_mode = 'page';
    }
    return isset($this->fields[$name]) ? extra_vars_get_field_value($name, $this->fields[$name], $view_mode) : '';
  }

}
