<?php

/**
 * @file
 *   Contain main callbacks for extra vars value edit functionaly.
 */

/**
 * Extra vars edit page menu callback.
 */
function extra_vars_edit_page() {
  $output = array();
  $groups = extra_vars_load_groups();

  if (empty($groups)) {
    return _extra_vars_empty();
  }
  $fields = extra_vars_load_fields();
  $head = array(t('Name'), t('Value'), t('Use'));
  $output['tables'] = array(
    '#prefix' => '<div id="extra-vars-table-group">',
    '#suffix' => '</div>',
  );
  $group_action_link_options = array('query' => drupal_get_destination());
  foreach ($groups as $gid => $group) {
    $group_action_label = t('Edit !group vars', array('!group' => $group->name));
    $group_action_path = 'admin/content/extra-vars/' . $gid;
    $output['tables'][$gid] = array(
      '#prefix' => '<div class="extra-vars-table-wrap">',
      '#suffix' => '</div>',
      'actions' => array(
        '#prefix' => '<div class="extra-vars-actions">',
        '#suffix' => '</div>',
        '#markup' => l($group_action_label, $group_action_path, $group_action_link_options),
      ),
      'table' => array(
        '#theme' => 'table',
        '#header' => $head,
        '#rows' => array(),
      )
    );
  }
  foreach ($fields as $fid => $field) {
    $value = extra_vars_get_field_value($field->machine_name, $field->type, 'admin');
    if (empty($value)) {
      $value = t('-- empty value --');
    }

    $output['tables'][$field->gid]['table']['#rows'][] = array(
      $field->name . '<br><em><small>' . $field->description . '</small></em>', $value, _extra_vars_helper_text($field->machine_name),
    );
  }
  foreach (element_children($output['tables']) as $gid) {
    if (empty($output['tables'][$gid]['table']['#rows'])) {
      unset($output['tables'][$gid]);
    }
  }
  $output['help'] = array(
    '#prefix' => '<p><i>',
    '#suffix' => '</i></p>',
    '#markup' => t('You can use vars vith specific view_mode. Ex: [extra_vars:varname:VIEW_MODE] on text editor or $extra_vars->{\'varname:VIEW_MODE\'} in tpl files.'),
  );
  $output['tables']['#attached']['css'][] = drupal_get_path('module', 'extra_vars') . '/theme/admin_table.css';
  return $output;
}
/**
 * Extra vars empty text callback.
 */
function _extra_vars_empty() {
  $output['#markup'] = t('First create groups and variables.');
  return $output;
}

/**
 * Extra vars group edit page form wrapper.
 */
function extra_vars_group_edit_page($group) {
  return drupal_get_form('variable_group_form', 'extra_vars_' . $group->gid);
}
