<?php

/**
 * Administration settigns form callback.
 */
function extra_vars_settings_form($form, $form_state) {
  $form['data'] = extra_vars_get_data_table();
  $groups = $fields = extra_vars_load_fields();
  $form['group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Create group'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
      ) + extra_vars_group_form($form, $form_state);
  $form['#validate'] = $form['group']['#validate'];
  unset($form['group']['#validate']);
  $form['group']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
    '#submit' => array('extra_vars_create_group_submit'),
  );
  return $form;
}

function extra_vars_create_group_submit(&$form, &$form_state) {
  form_state_values_clean($form_state);
  if (isset($form_state['values']['group']['actions'])) {
    unset($form_state['values']['group']['actions']);
  }
  $group = (object) $form_state['values']['group'];
  extra_vars_group_save($group);
  drupal_set_message(t('Group %name created', array('%name' => $group->name)));
}

function extra_vars_get_data_table() {
  $groups = extra_vars_load_groups();
  $form = array();
  foreach ($groups as $gid => $group) {
    $form[$gid] = array(
      '#type' => 'fieldset',
      '#title' => $group->name,
    );
    $fields = extra_vars_load_fields(array(), array('gid' => $gid));
    $form[$gid]['fields'] = extra_vars_get_fields_table($fields);
    $form[$gid]['links'] = array(
      '#weight' => 20,
      '#theme' => 'links',
      '#links' => array(
        array(
          'title' => t('Add field'),
          'href' => 'admin/structure/extra-vars/fields/add',
          'query' => array('group' => $gid),
        ),
        array(
          'title' => t('Edit group'),
          'href' => 'admin/structure/extra-vars/groups/' . $gid . '/edit',
        ),
        array(
          'title' => t('Delete group'),
          'href' => 'admin/structure/extra-vars/groups/' . $gid . '/delete',
        ),
      ),
    );
  }

  return $form;
}

function extra_vars_field_form($form, $form_state, $field = NULL) {
  $query = drupal_get_query_parameters();
  if (!$field && isset($query['group'])) {
    $gid = (int) $query['group'];
  }
  if ($field) {
    $gid = $field->gid;
  }
  $form['gid'] = array(
    '#type' => 'select',
    '#default_value' => isset($gid) ? $gid : NULL,
    '#options' => extra_vars_groups_list(),
    '#required' => TRUE,
    '#weight' => 20,
    '#title' => t('Select group'),
    '#empty_option' => t('- Select -'),
  );
  if (isset($field->fid)) {
    $form['fid'] = array(
      '#type' => 'value',
      '#value' => $field->fid,
    );
  }
  $form['name'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Field name'),
    '#default_value' => isset($field->name) ? $field->name : '',
  );
  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#required' => TRUE,
    '#title' => t('Field name'),
    '#default_value' => isset($field->machine_name) ? $field->machine_name : '',
    '#machine_name' => array(
      'exists' => 'extra_vars_field_exists',
      'source' => array('name'),
      'label' => t('Field machine name'),
      'replace_pattern' => '[^a-z0-9-]+',
      'replace' => '-',
    ),
    '#disabled' => !empty($field->machine_name),
  );
  $form['description'] = array(
    '#type' => 'textfield',
    '#required' => FALSE,
    '#title' => t('Description'),
    '#default_value' => isset($field->description) ? $field->description : '',
  );
  $form['type'] = array(
    '#title' => t('Field type'),
    '#type' => 'select',
    '#options' => extra_vars_get_types_list(),
    '#default_value' => isset($field->type) ? $field->type : NULL,
    '#required' => TRUE,
    '#empty_option' => t('- Select -'),
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#delta' => 20,
    '#default_value' => isset($field->weight) ? $field->weight : 0,
    '#weight' => 30
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => isset($field->fid) ? t('Edit') : t('Save'),
    '#submit' => array('extra_vars_fields_form_submit'),
  );
  return $form;
}

function extra_vars_fields_form_submit(&$form, &$form_state) {
  form_state_values_clean($form_state);
  $field = (object) $form_state['values'];
  if (empty($field->fid)) {
    drupal_set_message(t('Field %name created', array('%name' => $field->name)));
  }
  else {
    drupal_set_message(t('Field %name updated', array('%name' => $field->name)));
  }
  extra_vars_field_save($field);

  $form_state['redirect'] = 'admin/structure/extra-vars';
  cache_clear_all('*', 'cache_variable', TRUE);
}

function extra_vars_get_fields_table($fields) {
  if (empty($fields)) {
    return array('#markup' => t('Fields not exists'));
  }
  $header = array(t('Field name'), t('Field type'), t('Actions'));
  $rows = array();
  $types = extra_vars_get_types_list();
  foreach ($fields as $fid => $field) {
    $actions = array(
      'edit' => array(
        'title' => t('Edit'),
        'href' => 'admin/structure/extra-vars/fields/' . $fid . '/edit'
      ),
      'delete' => array(
        'title' => t('Delete'),
        'href' => 'admin/structure/extra-vars/fields/' . $fid . '/delete'
      ),
    );
    $rows[$fid] = array(
      $field->name,
      $types[$field->type],
      theme('links', array('links' => $actions)),
    );
  }
  return array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
}

function extra_vars_field_delete_form($form, &$form_state, $field) {
  $form['#field'] = $field;
  $form['fid'] = array('#type' => 'value', '#value' => $field->fid);
  return confirm_form($form, t('Are you sure you want to delete field %name?', array('%name' => $field->name)), 'admin/structure/extra-vars', t('This action cannot be undone.'), t('Delete'), t('Cancel')
  );
}

function extra_vars_field_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    extra_vars_field_delete($form_state['values']['fid']);
  }
  $form_state['redirect'] = 'admin/structure/extra-vars';
}

function extra_vars_group_form($form, $form_state, $group = NULL) {
  $form = array(
    '#type' => 'fieldset',
    '#title' => t('Create group'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Group name'),
    '#default_value' => isset($group->name) ? $group->name : '',
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#delta' => 20,
    '#default_value' => isset($group->weight) ? $group->weight : 0,
  );
  if (isset($group->gid)) {
    $form['gid'] = array(
      '#type' => 'value',
      '#value' => $group->gid,
    );
  }
  $form['#validate'][] = 'extra_vars_validate_group';
  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Edite'),
    '#submit' => array('extra_vars_edit_group_submit'),
  );
  return $form;
}

function extra_vars_edit_group_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/extra-vars';
  form_state_values_clean($form_state);
  unset($form_state['values']['actions']);
  $group = (object) $form_state['values'];
  extra_vars_group_save($group);
}

function extra_vars_validate_group($form, &$form_state) {
  $group = isset($form_state['values']['group']) ? $form_state['values']['group'] : $form_state['values'];

  $gid = isset($group['gid']) ? $group['gid'] : 0;
  if ($gid) {
    $field_name = 'name';
  }
  else {
    $field_name = 'group][name';
  }
  if (extra_vars_group_exists($group['name'], $gid)) {
    form_set_error($field_name, t('Group name must be unique'));
  }
}

function extra_vars_group_delete_form($form, &$form_state, $group) {
  $form['gid'] = array('#type' => 'value', '#value' => $group->gid);
  return confirm_form($form, t('Are you sure you want to delete group %name and all referenced fields?', array('%name' => $group->name)), 'admin/structure/extra-vars', t('This action cannot be undone.'), t('Delete'), t('Cancel')
  );
}

function extra_vars_group_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    extra_vars_group_delete($form_state['values']['gid']);
  }
  $form_state['redirect'] = 'admin/structure/extra-vars';
}
